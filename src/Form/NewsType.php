<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre',null,array("attr"=>["class"=>"form-control"]))
            ->add('annee',null,array("attr"=>["class"=>"form-control"]))
            ->add('support',null,array("attr"=>["class"=>"form-control"]))
            ->add('document',FileType::class,array("required"=>false,"data_class"=>null))
            ->add('auteurs',null,array("attr"=>["class"=>"form-control"]))
            ->add('resume',TextareaType::class,array("attr"=>["class"=>"form-control"]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
