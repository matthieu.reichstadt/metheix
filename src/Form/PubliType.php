<?php

namespace App\Form;

use App\Entity\Publi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PubliType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source',null,array("attr"=>["class"=>"form-control"]))
            ->add('location',null,array("attr"=>["class"=>"form-control"]))
            ->add('year',null,array("attr"=>["class"=>"form-control"]))
            ->add('code',null,array("attr"=>["class"=>"form-control"]))
            ->add('auteur',null,array("attr"=>["class"=>"form-control"]))
            ->add('groupe',null,array("attr"=>["class"=>"form-control"]))
            ->add('datatype',null,array("attr"=>["class"=>"form-control"]))
            ->add('pedigree',null,array("attr"=>["class"=>"form-control"]))
            ->add('trial',null,array("attr"=>["class"=>"form-control"]))
            ->add('exp',null,array("attr"=>["class"=>"form-control"]))
            ->add('trt',null,array("attr"=>["class"=>"form-control"]))
            ->add('n',null,array("attr"=>["class"=>"form-control"]))
            ->add('species',null,array("attr"=>["class"=>"form-control"]))
            ->add('dietcode',null,array("attr"=>["class"=>"form-control"]))
            ->add('status',null,array("attr"=>["class"=>"form-control"]))
            ->add('dim',null,array("attr"=>["class"=>"form-control"]))
            ->add('meatAgeStart',null,array("attr"=>["class"=>"form-control"]))
            ->add('meatAgeEnd',null,array("attr"=>["class"=>"form-control"]))
            ->add('meatDuration',null,array("attr"=>["class"=>"form-control"]))
            ->add('meatADG',null,array("attr"=>["class"=>"form-control"]))
            ->add('meatCarcDG',null,array("attr"=>["class"=>"form-control"]))
            ->add('feedof',null,array("attr"=>["class"=>"form-control"]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Publi::class,
        ]);
    }
}
