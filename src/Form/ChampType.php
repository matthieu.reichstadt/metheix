<?php

namespace App\Form;

use App\Entity\Champ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ChampType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle',null,array("attr"=>["class"=>"form-control"]))
            ->add('position',null,array("attr"=>["class"=>"form-control"]))
            ->add('positionCeders',null,array("attr"=>["class"=>"form-control"]))
            ->add('type',null,array("attr"=>["class"=>"select2 form-control"]))
            ->add('display',null,array("attr"=>["class"=>"form-control"]))
            ->add('description',TextAreaType::class,array("required"=>false,"attr"=>["class"=>"form-control"]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Champ::class,
        ]);
    }
}
