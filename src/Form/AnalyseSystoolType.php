<?php

namespace App\Form;

use App\Entity\AnalyseSystool;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AnalyseSystoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ration', FileType::class,array("required"=>false,"data_class"=>null))
            ->add('aliment', FileType::class,array("required"=>false,"data_class"=>null))
            ->add('date',DateType::class,array(
               "widget" => "single_text",
               "format" => "dd/MM/yyyy",
               "attr"=>array("class"=>"datePicker form-control"),
               "required"=>false
                )
            )
            ->add('sortie',null,array("attr"=>["class"=>"form-control"]))
            ->add('nom',null,array("attr"=>["class"=>"form-control"]))
            ->add('user',null,array("attr"=>["readonly"=>true,"class"=>"form-control"]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnalyseSystool::class,
        ]);
    }
}
