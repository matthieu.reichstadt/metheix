<?php

namespace App\Controller;

use App\Entity\AnalyseSystool;
use App\Form\AnalyseSystoolType;
use App\Repository\AnalyseSystoolRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\AnalyseSystoolFilterType;
use App\Filter\Entity\AnalyseSystoolSearch;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Security;
/**
 * @Route("/analysesystool")
 */
class AnalyseSystoolController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="analyse_systool_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(AnalyseSystoolRepository $analyseSystoolRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {

        $search = new AnalyseSystoolSearch();
        $form=$this->createForm(AnalyseSystoolFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $analyseSystoolRepository->findAllVisibleQuery($search);
        $nb=count($entities);

        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );
        $file_exists=array();
        foreach ($entities as $analyseSystool) $file_exists[$analyseSystool->getId()] = file_exists(__DIR__."/../../public/uploads/analyseSystool/".$analyseSystool->getSortie());

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'file_exists' => $file_exists,
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("analyse_systool/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'publi-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;

    }

    /**
     * @Route("/new", name="analyse_systool_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $analyseSystool = new AnalyseSystool();
        $user = $this->getUser();
        $analyseSystool->setUser($user);
        $form = $this->createForm(AnalyseSystoolType::class, $analyseSystool);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($analyseSystool->getRation() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_systool_directory'),$form['ration']);
                $fileUploader->upload();
                $analyseSystool->setRation($fileUploader->getFileName());
            }
            if ($analyseSystool->getAliment() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_systool_directory'),$form['aliment']);
                $fileUploader->upload();
                $analyseSystool->setAliment($fileUploader->getFileName());
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($analyseSystool);
            $entityManager->flush();
            $this->addFlash('success','Record successfully saved');

            return $this->redirectToRoute('analyse_systool_index');
        }

        return $this->render('analyse_systool/new.html.twig', [
            'analyse_systool' => $analyseSystool,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="analyse_systool_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnalyseSystool $analyseSystool): Response
    {
        $this->get('session')->set('analysesystool_ration', ($analyseSystool->getRation()));
        $this->get('session')->set('analysesystool_aliment', ($analyseSystool->getAliment()));
        $form = $this->createForm(AnalyseSystoolType::class, $analyseSystool);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($analyseSystool->getRation() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_systool_directory'),$form['ration']);
                $fileUploader->upload();
                $analyseSystool->setRation($fileUploader->getFileName());
                if ($this->get('session')->get('analysesystool_ration') != '' && file_exists(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_ration')))                       
                   unlink(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_ration'));
            }
            // si on veut garder l'ancien fichier
            elseif (!isset($_POST["supprration"]))    $analyseSystool->setRation($this->get('session')->get('analysesystool_ration'));
            // si on veut supprimer l'ancien fichier
            elseif ($this->get('session')->get('analysesystool_ration') != '' && file_exists(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_ration')))                                
               unlink(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_ration'));

            if ($analyseSystool->getAliment() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_systool_directory'),$form['aliment']);
                $fileUploader->upload();
                $analyseSystool->setAliment($fileUploader->getFileName());
                if ($this->get('session')->get('analysesystool_aliment') != '' && file_exists(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_aliment')))                       
                   unlink(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_aliment'));
            }
            // si on veut garder l'ancien fichier
            elseif (!isset($_POST["suppraliment"]))    $analyseSystool->setAliment($this->get('session')->get('analysesystool_aliment'));
            // si on veut supprimer l'ancien fichier
            elseif ($this->get('session')->get('analysesystool_aliment') != '' && file_exists(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_aliment')))                                
               unlink(__DIR__."/../../public/uploads/analyseSystool/".$this->get('session')->get('analysesystool_aliment'));

            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->remove('analysesystool_ration');
            $this->get('session')->remove('analysesystool_aliment');

            return $this->redirectToRoute('analyse_systool_index');
        }

        return $this->render('analyse_systool/edit.html.twig', [
            'analyse_systool' => $analyseSystool,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="analyse_systool_delete", methods={"GET"})
     */
    public function delete(Request $request, AnalyseSystool $analyseSystool): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$analyseSystool->getId(), $request->request->get('_token'))) {
            if ($analyseSystool->getRation()!='') unlink(__DIR__."/../../public/uploads/analyseSystool/".$protocole->getRation());
            if ($analyseSystool->getAliment()!='') unlink(__DIR__."/../../public/uploads/analyseSystool/".$protocole->getAliment());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($analyseSystool);
            $entityManager->flush();
        // }

        return $this->redirectToRoute('analyse_systool_index');
    }
}
