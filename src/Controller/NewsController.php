<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Security;
/**
 * @Route("/news")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/", name="news_index", methods={"GET"})
     */
    public function index(NewsRepository $newsRepository,Request $request,PaginatorInterface $paginator): Response
    {
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $newsRepository->findAll();
        $nb=count($entities);

        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );
        $file_exists=array();
        foreach ($entities as $new) $file_exists[$new->getId()] = file_exists(__DIR__."/../../public/uploads/news/".$new->getDocument());

        $data=[
            'file_exists' => $file_exists,
            'entities' => $entitiesPaginated,
            'nb' => $nb,
            'maxItemPerPage' =>$maxItemPerPage,
        ];
        $rep=$this->render("news/index.html.twig", $data);
        return $rep;
    }

    /**
     * @Route("/new", name="news_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($news->getDocument() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('news_directory'),$form['document']);
                $fileUploader->upload();
                $news->setDocument($fileUploader->getFileName());
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();
            $this->addFlash('success','Record successfully saved');

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_show", methods={"GET"})
     */
    public function show(News $news): Response
    {
        $file_exists=array();
        $file_exists[$news->getId()] = file_exists(__DIR__."/../../public/uploads/news/".$news->getDocument());
        return $this->render('news/show.html.twig', [
            'file_exists' => $file_exists,
            'news' => $news,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="news_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, News $news): Response
    {
        $this->get('session')->set('news_document', ($news->getDocument()));
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($news->getDocument() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('news_directory'),$form['document']);
                $fileUploader->upload();
                $news->setDocument($fileUploader->getFileName());
                if ($this->get('session')->get('news_document') != '' && file_exists(__DIR__."/../../public/uploads/news/".$this->get('session')->get('news_document')))                       
                   unlink(__DIR__."/../../public/uploads/news/".$this->get('session')->get('news_document'));
            }
            // si on veut garder l'ancien fichier
            elseif (!isset($_POST["suppr"]))    $news->setDocument($this->get('session')->get('news_document'));
            // si on veut supprimer l'ancien fichier
            elseif ($this->get('session')->get('news_document') != '' && file_exists(__DIR__."/../../public/uploads/news/".$this->get('session')->get('news_document')))                                
               unlink(__DIR__."/../../public/uploads/news/".$this->get('session')->get('news_document'));

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Record successfully updated');

            $this->get('session')->remove('news_document');
            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="news_delete", methods={"GET"})
     */
    public function delete(Request $request, News $news): Response
    {
            if ($news->getDocument()!='') unlink(__DIR__."/../../public/uploads/news/".$protocole->getDocument());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($news);
            $entityManager->flush();
            $this->addFlash('err','Record successfully deleted');

        return $this->redirectToRoute('news_index');
    }
}
