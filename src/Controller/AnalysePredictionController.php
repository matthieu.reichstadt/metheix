<?php

namespace App\Controller;

use App\Entity\AnalysePrediction;
use App\Form\AnalysePredictionType;
use App\Repository\AnalysePredictionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Filter\Form\AnalysePredictionFilterType;
use App\Filter\Entity\AnalysePredictionSearch;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Security;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
/**
 * @Route("/analyseprediction")
 */
class AnalysePredictionController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="analyse_prediction_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(AnalysePredictionRepository $analysePredictionRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new AnalysePredictionSearch();
        $form=$this->createForm(AnalysePredictionFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $analysePredictionRepository->findAllVisibleQuery($search);
        $nb=count($entities);

        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );
        $file_exists=array();
        foreach ($entities as $analysePrediction) $file_exists[$analysePrediction->getId()] = file_exists(__DIR__."/../../public/uploads/analysePrediction/".$analysePrediction->getSortie());

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'file_exists' => $file_exists,
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("analyse_prediction/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'publi-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;
    }

    /**
     * @Route("/new", name="analyse_prediction_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $analysePrediction = new AnalysePrediction();
        $user = $this->getUser();
        $analysePrediction->setUser($user);
        $form = $this->createForm(AnalysePredictionType::class, $analysePrediction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($analysePrediction->getInput() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_prediction_directory'),$form['input']);
                $fileUploader->upload();
                $analysePrediction->setInput($fileUploader->getFileName());
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($analysePrediction);
            $entityManager->flush();

            return $this->redirectToRoute('analyse_prediction_index');
        }

        return $this->render('analyse_prediction/new.html.twig', [
            'analyse_prediction' => $analysePrediction,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="analyse_prediction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnalysePrediction $analysePrediction): Response
    {
        $this->get('session')->set('analyseprediction_input', ($analysePrediction->getInput()));
        $form = $this->createForm(AnalysePredictionType::class, $analysePrediction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($analysePrediction->getInput() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_prediction_directory'),$form['input']);
                $fileUploader->upload();
                $analysePrediction->setInput($fileUploader->getFileName());
                if ($this->get('session')->get('analyseprediction_input') != '' && file_exists(__DIR__."/../../public/uploads/analysePrediction/".$this->get('session')->get('analyseprediction_input')))                       
                   unlink(__DIR__."/../../public/uploads/analysePrediction/".$this->get('session')->get('analyseprediction_input'));
            }
            // si on veut garder l'ancien fichier
            elseif (!isset($_POST["suppr"]))    $analysePrediction->setInput($this->get('session')->get('analyseprediction_input'));
            // si on veut supprimer l'ancien fichier
            elseif ($this->get('session')->get('analyseprediction') != '' && file_exists(__DIR__."/../../public/uploads/analysePrediction/".$this->get('session')->get('analyseprediction_input')))                                
               unlink(__DIR__."/../../public/uploads/analysePrediction/".$this->get('session')->get('analyseprediction_input'));

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->remove('analyseprediction_input');
            return $this->redirectToRoute('analyse_prediction_index');
        }

        return $this->render('analyse_prediction/edit.html.twig', [
            'analyse_prediction' => $analysePrediction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="analyse_prediction_delete", methods={"GET"})
     */
    public function delete(Request $request, AnalysePrediction $analysePrediction): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$analysePrediction->getId(), $request->request->get('_token'))) {
            if ($analysePrediction->getInput()!='') unlink(__DIR__."/../../public/uploads/analysePrediction/".$protocole->getInput());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($analysePrediction);
            $entityManager->flush();
        // }

        return $this->redirectToRoute('analyse_prediction_index');
    }
}
