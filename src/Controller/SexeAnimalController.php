<?php

namespace App\Controller;

use App\Entity\SexeAnimal;
use App\Form\SexeAnimalType;
use App\Repository\SexeAnimalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\SexeAnimalFilterType;
use App\Filter\Entity\SexeAnimalSearch;

/**
 * @Route("/sexeanimal")
 */
class SexeAnimalController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="sexeanimal_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(SexeAnimalRepository $sexeanimalRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new SexeAnimalSearch();
        $form=$this->createForm(SexeAnimalFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $sexeanimalRepository->findAllVisibleQuery($search);
        $nb=count($entities);

        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("sexeanimal/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'sexeanimal-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        return $rep;
    }

    /**
     * @Route("/new", name="sexeanimal_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entity = new SexeAnimal();
        $form = $this->createForm(SexeAnimalType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
            $this->addFlash('success','Record successfully saved');
            return $this->redirectToRoute('sexeanimal_index');
        }

        return $this->render('sexeanimal/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sexeanimal_show", methods={"GET"})
     */
    public function show(SexeAnimal $entity): Response
    {
        return $this->render('SexeAnimalMin#/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sexeanimal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SexeAnimal $entity): Response
    {
        $form = $this->createForm(SexeAnimalType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Record successfully updated');

            return $this->redirectToRoute('sexeanimal_index');
        }

        return $this->render('sexeanimal/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="sexeanimal_delete", methods={"GET"})
     */
    public function delete(Request $request, SexeAnimal $entity): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
            $this->addFlash('err','Record successfully deleted');
        // }

        return $this->redirectToRoute('sexeanimal_index');
    }
}
