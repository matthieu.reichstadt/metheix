<?php

namespace App\Controller;

use App\Entity\BreedAnimal;
use App\Form\BreedAnimalType;
use App\Repository\BreedAnimalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\BreedAnimalFilterType;
use App\Filter\Entity\BreedAnimalSearch;

/**
 * @Route("/breedanimal")
 */
class BreedAnimalController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="breedanimal_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(BreedAnimalRepository $breedanimalRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new BreedAnimalSearch();
        $form=$this->createForm(BreedAnimalFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $breedanimalRepository->findAllVisibleQuery($search);
        $nb=count($entities);

        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("breedanimal/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'breedanimal-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        return $rep;
    }

    /**
     * @Route("/new", name="breedanimal_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entity = new BreedAnimal();
        $form = $this->createForm(BreedAnimalType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
            $this->addFlash('success','Record successfully saved');
            return $this->redirectToRoute('breedanimal_index');
        }

        return $this->render('breedanimal/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="breedanimal_show", methods={"GET"})
     */
    public function show(BreedAnimal $entity): Response
    {
        return $this->render('BreedAnimalMin#/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="breedanimal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BreedAnimal $entity): Response
    {
        $form = $this->createForm(BreedAnimalType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Record successfully updated');
            return $this->redirectToRoute('breedanimal_index');
        }

        return $this->render('breedanimal/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="breedanimal_delete", methods={"GET"})
     */
    public function delete(Request $request, BreedAnimal $entity): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
            $this->addFlash('err','Record successfully deleted');
        // }

        return $this->redirectToRoute('breedanimal_index');
    }
}
