<?php

namespace App\Controller;

use App\Entity\TypeChamp;
use App\Form\TypeChampType;
use App\Repository\TypeChampRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/champ")
 */
class TypeChampController extends AbstractController
{
    /**
     * @Route("/", name="type_champ_index", methods={"GET"})
     */
    public function index(TypeChampRepository $typeChampRepository): Response
    {
        return $this->render('type_champ/index.html.twig', [
            'type_champs' => $typeChampRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_champ_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeChamp = new TypeChamp();
        $form = $this->createForm(TypeChampType::class, $typeChamp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeChamp);
            $entityManager->flush();

            return $this->redirectToRoute('type_champ_index');
        }

        return $this->render('type_champ/new.html.twig', [
            'type_champ' => $typeChamp,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_champ_show", methods={"GET"})
     */
    public function show(TypeChamp $typeChamp): Response
    {
        return $this->render('type_champ/show.html.twig', [
            'type_champ' => $typeChamp,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_champ_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeChamp $typeChamp): Response
    {
        $form = $this->createForm(TypeChampType::class, $typeChamp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_champ_index');
        }

        return $this->render('type_champ/edit.html.twig', [
            'type_champ' => $typeChamp,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_champ_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeChamp $typeChamp): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeChamp->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeChamp);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_champ_index');
    }
}
