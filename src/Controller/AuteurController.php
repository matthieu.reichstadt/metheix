<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Form\AuteurType;
use App\Repository\AuteurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\AuteurFilterType;
use App\Filter\Entity\AuteurSearch;

/**
 * @Route("/auteur")
 */
class AuteurController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="auteur_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(AuteurRepository $auteurRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new AuteurSearch();
        $form=$this->createForm(AuteurFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $auteurRepository->findAllVisibleQuery($search);
        $nb=count($entities);

        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("auteur/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'auteur-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        return $rep;
    }

    /**
     * @Route("/new", name="auteur_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entity = new Auteur();
        $form = $this->createForm(AuteurType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
            $this->addFlash('success','Record successfully saved');

            return $this->redirectToRoute('auteur_index');
        }

        return $this->render('auteur/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="auteur_show", methods={"GET"})
     */
    public function show(Auteur $entity): Response
    {
        return $this->render('AuteurMin#/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="auteur_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Auteur $entity): Response
    {
        $form = $this->createForm(AuteurType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Record successfully updated');

            return $this->redirectToRoute('auteur_index');
        }

        return $this->render('auteur/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="auteur_delete", methods={"GET"})
     */
    public function delete(Request $request, Auteur $entity): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
            $this->addFlash('err','Record successfully deleted');
        // }

        return $this->redirectToRoute('auteur_index');
    }
}
