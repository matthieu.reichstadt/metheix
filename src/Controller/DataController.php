<?php

namespace App\Controller;

use App\Entity\Data;
use App\Form\DataType;
use App\Repository\DataRepository;
use App\Repository\PubliRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\DataFilterType;
use App\Filter\Entity\DataSearch;
/**
 * @Route("/data")
 */
class DataController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="data_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(DataRepository $dataRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new DataSearch();
        $form=$this->createForm(DataFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $dataRepository->findAllVisibleQuery($search);
        $nb=count($entities);
        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("data/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'data-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;
    }

    /**
     * @Route("/animal/{idPubli}", name="data_animal",methods={"GET","POST"})
     */
    public function animal($idPubli,DataRepository $dataRepository,Request $request,PaginatorInterface $paginator): Response
    {
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $dataRepository->findVisibleQueryByPubli("animal",$idPubli);
        $nb=count($entities);
        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
            ];
        $rep=$this->render("data/index_animal.html.twig", $data);
        return $rep;
    }

    /**
     * @Route("/diet/{idPubli}", name="data_diet",methods={"GET","POST"})
     */
    public function diet($idPubli,DataRepository $dataRepository,Request $request,PaginatorInterface $paginator): Response
    {
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $dataRepository->findVisibleQueryByPubli("diet",$idPubli);
        $nb=count($entities);
        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
            ];
        $rep=$this->render("data/index_animal.html.twig", $data);
        return $rep;
    }

    /**
     * @Route("/new", name="data_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $data = new Data();
        $form = $this->createForm(DataType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();

            return $this->redirectToRoute('data_index');
        }

        return $this->render('data/new.html.twig', [
            'data' => $data,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="data_show", methods={"GET"})
     */
    public function show(Data $data): Response
    {
        return $this->render('data/show.html.twig', [
            'data' => $data,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="data_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Data $data): Response
    {
        $form = $this->createForm(DataType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('data_index');
        }

        return $this->render('data/edit.html.twig', [
            'data' => $data,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="data_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Data $data): Response
    {
        if ($this->isCsrfTokenValid('delete'.$data->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($data);
            $entityManager->flush();
        }

        return $this->redirectToRoute('data_index');
    }
}
