<?php

namespace App\Controller;

use App\Entity\Publi;
use App\Form\PubliType;
use App\Repository\PubliRepository;
use App\Repository\DataRepository;
use App\Repository\ChampRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\PubliFilterType;
use App\Filter\Entity\PubliSearch;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @Route("/publi")
 */
class PubliController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="publi_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(PubliRepository $publiRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {       
        $session = new Session();
        if(null ===$request->query->get('page'))
        {
            $this->unsetSession();
        }
    
        $search = new PubliSearch();
        $form=$this->createForm(PubliFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $publiRepository->findAllVisibleQuery($search);
        $nb=count($entities);
        $session->set('entities',$entities);
        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("publi/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'publi-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;
    }
    
    /**
     * @Route("/selectExport/{id}", name="select_export", options={"expose"=true})
     * @Method({"GET","POST"})
     */
    public function selectExport(Request $request, Publi $entity): Response
    {
        $session = new Session();
        $lineExported = array();
        if(null !== $session->get('exportlist'))
        {
            $lineExported = $session->get('exportlist');
        }
        
        if (isset($lineExported[$entity->getId()])) {
            unset($lineExported[$entity->getId()]);
        }else{
            $lineExported[$entity->getId()] = $entity; 
        }
        $session->set('exportlist',$lineExported);
        return $this->json(['id' => $entity->getId()]);
    }
    
    /**
     * @Route("/export}", name="publi_export")
     * @Method({"GET","POST"})
     */
    public function export(Request $request,ChampRepository $champRepository, PubliRepository $publiRepository)
        : Response
    {
        $session = new Session();
        $lineExported = array();
        if(null !== $session->get('exportlist'))    $lineExported = $session->get('exportlist');
        if (count($lineExported)==0)                $lineExported = $session->get('entities');

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $worksheet = $spreadsheet->getActiveSheet();
        
        //create header
        $c=0;
        $range = $this->getcolumnrange('A','BBB');
        $header = array(
            $range[$c++]=>'Source',
            $range[$c++]=>'Location',
            $range[$c++]=>'Year',
            $range[$c++]=>'Code',
            $range[$c++]=>'Group',
            $range[$c++]=>'Author',
            $range[$c++]=>'Datatype',
            $range[$c++]=>'Pedigree',
            $range[$c++]=>'Trial',
            $range[$c++]=>'Exp',
            $range[$c++]=>'Trt',
            $range[$c++]=>'N',
            $range[$c++]=>'Species',
            $range[$c++]=>'DietCode',
            $range[$c++]=>'Status',
            $range[$c++]=>'Dim',
            $range[$c++]=>'MeatAgeStart',
            $range[$c++]=>'MeatAgeEnd',
            $range[$c++]=>'MeatDuration',
            $range[$c++]=>'MeatADG',
            $range[$c++]=>'MeatCarcDg',
            $range[$c++]=>'FeedOf',
        );
        
        $champs = $champRepository->findAll();
        
        $i=1;
        foreach($header as $key => $value){
            $worksheet->getCell($key.$i)->setValue($value);
        }
        foreach($champs as $champ){
            $worksheet->getCell($champ->getPosition().$i)->setValue($champ->getLibelle());
        }
        
        //create data
        foreach($lineExported as $publi)
        {
            $publiData = $publiRepository->find($publi->getId());
            
            $i++;
            $worksheet->getCell(array_search('Code', $header).$i)->setValue($publiData->getCode());
            $worksheet->getCell(array_search('Author', $header).$i)->setValue($publiData->getAuteur());
            $worksheet->getCell(array_search('Group', $header).$i)->setValue($publiData->getGroupe());
            $worksheet->getCell(array_search('Datatype', $header).$i)->setValue($publiData->getDatatype());
            $worksheet->getCell(array_search('Pedigree', $header).$i)->setValue($publiData->getPedigree());
            $worksheet->getCell(array_search('Trial', $header).$i)->setValue($publiData->getTrial());
            $worksheet->getCell(array_search('Exp', $header).$i)->setValue($publiData->getExp());
            $worksheet->getCell(array_search('Trt', $header).$i)->setValue($publiData->getTrt());
            $worksheet->getCell(array_search('N', $header).$i)->setValue($publiData->getN());
            $worksheet->getCell(array_search('Species', $header).$i)->setValue($publiData->getSpecies());
            $worksheet->getCell(array_search('DietCode', $header).$i)->setValue($publiData->getDietcode());
            $worksheet->getCell(array_search('Status', $header).$i)->setValue($publiData->getStatus());
            $worksheet->getCell(array_search('Dim', $header).$i)->setValue($publiData->getDim());
            $worksheet->getCell(array_search('MeatAgeStart', $header).$i)->setValue($publiData->getMeatAgeStart());
            $worksheet->getCell(array_search('MeatAgeEnd', $header).$i)->setValue($publiData->getMeatAgeEnd());
            $worksheet->getCell(array_search('MeatDuration', $header).$i)->setValue($publiData->getMeatDuration());
            $worksheet->getCell(array_search('MeatADG', $header).$i)->setValue($publiData->getMeatADG());
            $worksheet->getCell(array_search('MeatCarcDg', $header).$i)->setValue($publiData->getMeatCarcDG());
            $worksheet->getCell(array_search('Year', $header).$i)->setValue($publiData->getYear());
            $worksheet->getCell(array_search('Location', $header).$i)->setValue($publiData->getLocation());
            $worksheet->getCell(array_search('Source', $header).$i)->setValue($publiData->getSource());
            $worksheet->getCell(array_search('FeedOf', $header).$i)->setValue($publiData->getFeedof());
            
            foreach($publiData->getData() as $data)
            {
               $worksheet->getCell($data->getChamp()->getPosition().$i)->setValue($data->getValue());
            }
            
        }
        
        $response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="Export.xlsx"');
        $response->headers->set('Cache-Control','max-age=0');
        
        $this->unsetSession();
        return $response;
    }
    
    private function excelColumnRange($lower, $upper) {
        ++$upper;
        for ($i = $lower; $i !== $upper; ++$i) {
            yield $i;
        }
    }
    
    private function getcolumnrange($min,$max){
        $output = array();
        foreach ($this->excelColumnRange($min, $max) as $value) {
            array_push($output,$value);
        }   
        return $output;
    }

    /**
     * @Route("/new", name="publi_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entity = new Publi();
        $form = $this->createForm(PubliType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
            $this->addFlash('success','Record successfully saved');

            return $this->redirectToRoute('champ_new',['idPubli'=> $entity->getId()]);
        }

        return $this->render('publi/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="publi_show", methods={"GET"})
     */
    public function show(Publi $entity): Response
    {
        return $this->render('publi/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="publi_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Publi $entity): Response
    {
        $form = $this->createForm(PubliType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Record successfully updated');

            return $this->redirectToRoute('publi_index');
        }

        return $this->render('publi/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="publi_delete", methods={"GET"})
     */
    public function delete(Request $request, Publi $entity): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
            $this->addFlash('err','Record successfully deleted');
        // }

        return $this->redirectToRoute('publi_index');
    }
    
    private function unsetSession(){
        $session = new Session();
        if(null !== $session->get('exportlist'))
        {
            $session->set('exportlist',array());
        }
    }

    /**
     * @Route("/display/all.{_format}", name="publi_displaydata",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function displayData(DataRepository $dataRepository,ChampRepository $champRepository,PubliRepository $publiRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {       
        $session = new Session();
        if(null ===$request->query->get('page'))
        {
            $this->unsetSession();
        }
    
        $search = new PubliSearch();
        $form=$this->createForm(PubliFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $champs = $champRepository->findByDisplay(true);
        $publis = $publiRepository->findAll();
        $datas = $dataRepository->findAll();
        $entities=array();
        foreach ($publis as $publi)
        {
            foreach ($champs as $champ)
            {
                $data=$dataRepository->findBy(array('publi' => $publi, 'champ' =>$champ));
                if (count($data)>0)  $entities[$publi->getId()][$champ->getLibelle()]=$data[0]->getValue();
                else                $entities[$publi->getId()][$champ->getId()]="";
            }
        }
        // foreach($entities as $pub=>$listeDat)
        // {
        //     foreach ($listeDat as $chp=>$dat)
        //     {
        //         echo "$pub,$chp,$dat<br>";
        //     }
        // }
        // exit;
        $nb=count($publis);
        $session->set('entities',$publis);
        $entitiesPaginated = $paginator->paginate(
            $publis,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $publis
            ];
        else 
            $data=[
                'entities' => $entities,
                'champs' => $champs,
                'datas' => $datas,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("publi/display.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'publi-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;
    }
}
