<?php

namespace App\Controller;

use App\Entity\Champ;
use App\Form\ChampType;
use App\Repository\ChampRepository;
use App\Repository\DataRepository;
use App\Repository\PubliRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\ChampFilterType;
use App\Filter\Entity\ChampSearch;
use App\Filter\Form\ChampGrapheFilterType;
use App\Filter\Entity\ChampGrapheSearch;

/**
 * @Route("/champ")
 */
class ChampController extends AbstractController
{
    /**
     * @Route("/all.{_format}", name="champ_index",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function index(ChampRepository $champRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new ChampSearch();
        $form=$this->createForm(ChampFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entities = $champRepository->findAllVisibleQuery($search);
        $nb=count($entities);
        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("champ/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'champ-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;
    }

    /**
     * @Route("/recap.{_format}", name="champ_recap",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function recap(DataRepository $dataRepository,PubliRepository $publiRepository,ChampRepository $champRepository,Request $request,PaginatorInterface $paginator,$_format): Response
    {
        $search = new ChampSearch();
        $form=$this->createForm(ChampFilterType::class,$search);
        $form->handleRequest($request);
        $maxItemPerPage = $request->get('maxItemPerPage');
        if ($maxItemPerPage=="")    $maxItemPerPage=10;
        $entitiesPubli=$publiRepository->findAll();
        $nbPubli=count($entitiesPubli);
        $entities = $champRepository->findAllDisplayQuery($search);
        $liste=array();
        foreach ($entities as $champ)
        {
            $datas=$dataRepository->findByChamp($champ);
            $nb=count($datas);
            if ($nb>0)
            {
                $moyenne=0;
                $ecartType=0;
                $min=9999;
                $max=0;
                $q1=0;
                $mediane=0;
                $q3=0;
                $total=0;
                foreach ($datas as $data)
                {
                    if (is_numeric($data->getValue()))
                    {
                        $total+=$data->getValue();
                        if ($data->getValue()>=$max)  $max=$data->getValue();
                        if ($data->getValue()<=$min)  $min=$data->getValue();
                    }
                }
                $moyenne=$total/$nb;
                $ecart = [];
                foreach ($datas as $data) {
                    if (is_numeric($data->getValue())) {
                        //écart entre la valeur et la moyenne
                        $ecart_donnee = $data->getValue() - $moyenne;
                        //carré de l'écart
                        $ecart_donnee_carre = bcpow($ecart_donnee, 2, 2);
                        //Insertion dans le tableau
                        array_push($ecart, $ecart_donnee_carre);
                    }
                }
                $somme_ecart = array_sum($ecart);
                $division = $somme_ecart / $nb;
                $ecart_type = bcsqrt ($division, 2);
                if ($total>0) 
                {
                    echo $champ->getLibelle().'=>';
                    echo "nb:".count($datas).", moyenne:$moyenne, ecart-type:$ecart_type,  min:$min, max:$max, total: $total";
                    echo "<br>";
            
                }
            }
        }exit;
        $nb=count($entities);
        $entitiesPaginated = $paginator->paginate(
            $entities,
            $request->query->getInt('page', 1),
            $maxItemPerPage
        );

        if ($_format == 'csv') 
            $data=[
                'entities' => $entities
            ];
        else 
            $data=[
                'entities' => $entitiesPaginated,
                'nb' => $nb,
                'maxItemPerPage' =>$maxItemPerPage,
                'form'=>$form->createView()
            ];
        $rep=$this->render("champ/index.$_format.twig", $data);
        if ($_format == 'csv'){
            $fic = 'champ-'. \date("d-m-Y") . '.csv';
            $rep->headers->set('Content-Disposition','attachment; filename="'.$fic.'"');
        }
        
        return $rep;
    }



    /**
     * @Route("/new", name="champ_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $champ = new Champ();
        $form = $this->createForm(ChampType::class, $champ);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($champ);
            $entityManager->flush();

            return $this->redirectToRoute('champ_index');
        }

        return $this->render('champ/new.html.twig', [
            'champ' => $champ,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="champ_show", methods={"GET"})
     */
    public function show(Champ $champ): Response
    {
        return $this->render('champ/show.html.twig', [
            'champ' => $champ,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="champ_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Champ $champ): Response
    {
        $form = $this->createForm(ChampType::class, $champ);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('champ_index');
        }

        return $this->render('champ/edit.html.twig', [
            'champ' => $champ,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="champ_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Champ $champ): Response
    {
        if ($this->isCsrfTokenValid('delete'.$champ->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($champ);
            $entityManager->flush();
        }

        return $this->redirectToRoute('champ_index');
    }

    /**
     * @Route("/graphe/all.{_format}", name="champ_graphe",
     * requirements={"_format"="html|csv"},
     * defaults={"_format"="html"}))
     * @Method({"GET","POST"})
     */
    public function graphe(DataRepository $dataRepository,ChampRepository $champRepository,Request $request,$_format): Response
    {
        $search = new ChampGrapheSearch();
        $form=$this->createForm(ChampGrapheFilterType::class,$search);
        $form->handleRequest($request);
        $dataTot=array();
        $dataVar3=array();
        list($champ1,$champ2,$champ3,$entityChamp1,$entityChamp2,$entityChamp3)="";
        if ($form->isSubmitted() && $form->isValid()) {    
            $submitted="on";
            // entités champs
            $entityChamp1=$search->getVar1();
            $entityChamp2=$search->getVar2();
            $entityChamp3=$search->getVar3();
            // id de chacune des entités
            $idChamp1=$champRepository->find($entityChamp1->getId());
            $idChamp2=$champRepository->find($entityChamp2->getId());
            $idChamp3=$champRepository->find($entityChamp3->getId());
            // liste des datas dispos pour ces champs
            $arrayData1 = $dataRepository->findByChamp($idChamp1);
            $arrayData2 = $dataRepository->findByChamp($idChamp2);
            $arrayData3 = $dataRepository->findByChamp($idChamp3);
            foreach($arrayData1 as $data1)
            {
                foreach($arrayData2 as $data2)
                {
                    if ($data1->getPubli() == $data2->getPubli())
                    {
                        foreach ($arrayData3 as $data3)
                        {
                            if ($data3->getPubli() == $data1->getPubli())
                            {
                                // si les 3 champs sont présents dans la meme publi alors on envoie dans le tableau datatot
                                // si c'est le 1er envoi on génère le tableau, sinon on implémente
                                if (!isset($dataTot[$data3->getValue()])) $dataTot[$data3->getValue()]=array($data1->getValue().'|'.$data2->getValue());
                                else array_push($dataTot[$data3->getValue()],$data1->getValue().'|'.$data2->getValue());
                                if (!in_array($data3->getValue(),$dataVar3))    array_push($dataVar3,$data3->getValue());
                            }
                        }
                    }
                }
            }
        }
        else $submitted="off";
        // echo count($dataTot);
        // foreach ($dataVar3 as $val) echo "$val<br>";
        // echo "<hr>";
        // foreach ($dataTot as $key=>$tab)
        // {
        //     echo "$key<br>";
        //     foreach ($tab as $val)  echo "$val<br>";
        //     echo "<br>";
        // }
        $data=[
            'submitted'=>$submitted,
            'form' => $form->createView(),
            'dataVar3' => $dataVar3,
            'dataTot' => $dataTot,
            'var1' => $entityChamp1,
            'var2' => $entityChamp2,
            'var3' => $entityChamp3,
        ];
        $rep=$this->render("champ/graphe.html.twig", $data);
        return $rep;
    }
}
