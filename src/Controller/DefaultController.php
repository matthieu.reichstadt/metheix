<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PubliRepository;
use App\Repository\NewsRepository;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(NewsRepository $newsRepository)
    {
        $entitiesNews=$newsRepository->getLast5();
        return $this->render('index.html.twig',['entitiesNews' => $entitiesNews]);
    }


    /**
     * @Route("/communications", name="accueil_comm")
     */
    public function indexComm(NewsRepository $newsRepository)
    {
        $entitiesNews=$newsRepository->findAll();
        return $this->render('indexComm.html.twig',['entitiesNews' => $entitiesNews]);
    }

    /**
     * @Route("/data", name="accueil_donnees")
     */
    public function indexDonnees(NewsRepository $newsRepository,PubliRepository $publiRepository)
    {
        $entities = $publiRepository->findAll();
        $tabPubli=array();
        foreach ($entities as $entity)
        {
            $year=$entity->getYear();
            if (is_numeric($year)) {
                if (isset($tabPubli[$year]))    $tabPubli[$year]++;
                else                            $tabPubli[$year]=1;
            }
        }
        $entitiesNews=$newsRepository->getLast5();
        ksort($tabPubli);
        return $this->render('indexData.html.twig',['tabPubli' => $tabPubli,'entitiesNews' => $entitiesNews]);
    }
}
