<?php

namespace App\Controller;

use App\Entity\Equation;
use App\Form\EquationType;
use App\Repository\EquationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/equation")
 */
class EquationController extends AbstractController
{
    /**
     * @Route("/", name="equation_index", methods={"GET"})
     */
    public function index(EquationRepository $equationRepository): Response
    {
        return $this->render('equation/index.html.twig', [
            'equations' => $equationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="equation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $equation = new Equation();
        $form = $this->createForm(EquationType::class, $equation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($equation);
            $entityManager->flush();

            return $this->redirectToRoute('equation_index');
        }

        return $this->render('equation/new.html.twig', [
            'equation' => $equation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="equation_show", methods={"GET"})
     */
    public function show(Equation $equation): Response
    {
        return $this->render('equation/show.html.twig', [
            'equation' => $equation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="equation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Equation $equation): Response
    {
        $form = $this->createForm(EquationType::class, $equation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('equation_index');
        }

        return $this->render('equation/edit.html.twig', [
            'equation' => $equation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="equation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Equation $equation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$equation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($equation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('equation_index');
    }
}
