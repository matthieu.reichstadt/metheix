<?php

namespace App\Controller;

use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Filter\Form\UnitFilterType;
use App\Filter\Entity\UnitSearch;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use App\Filter\MyReadFilter;

use App\Entity\Champ;
use App\Entity\Data;
use App\Entity\Publi;

use App\Constante\ExcelConstante;

/**
 * @Route("/upload")
 */
class UploadController extends AbstractController
{
	
    /**
     * @Route("/", name="upload_index")
     */
    public function upload(Request $request)
    {			
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);
 		$upload = false;
		
        if ($form->isSubmitted() && $form->isValid()) {
			
           	$data = $form->getData();
		   
 		   	$file = $data['file'];
		   
 		   	$fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
		   
 		   	$file->move( 'excels',  $fileName );
			
 			$filterSubset = new MyReadFilter();
		
 			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
 			$reader->setReadDataOnly(true);
 			$reader->setReadFilter($filterSubset);
 			$spreadsheet = $reader->load('excels/'.$fileName);
					   
			$worksheet = $spreadsheet->getSheetByName("All data ver2");

			$em = $this->getDoctrine()->getManager();

 		    $listChamps = $this->getDoctrine()->getRepository(Champ::class)->findAll();
			$highestRow = $worksheet->getHighestDataRow();
			
			$range = $this->getcolumnrange('A','Z');

			for($row = 2; $row < $highestRow; $row++){
				$c=0;
				$publi = $this->getDoctrine()->getRepository(Publi::class)->findOneBy(
					[
						'code' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'auteur' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'groupe' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'datatype' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'pedigree' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'trial' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'exp' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'trt' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'n' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'species' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'dietcode' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'status' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'dim' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'meatAgeStart' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'meatAgeEnd' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'meatDuration' => $worksheet->getCell($range[$c++].$row)->getValue(),						
						'meatADG' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'meatCarcDG' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'year' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'location' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'source' => $worksheet->getCell($range[$c++].$row)->getValue(),
						'feedof' => $worksheet->getCell($range[$c++].$row)->getValue()
					]
				);

				$c=0;
				if($publi == null)
				{
					$publi = new Publi();
					$code=$worksheet->getCell($range[$c++].$row)->getValue();
					$publi->setCode($code);
					$publi->setAuteur($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setGroupe($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setDatatype($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setPedigree($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setTrial($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setExp($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setTrt($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setN($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setSpecies($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setDietCode($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setStatus($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setDim($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setMeatAgeStart($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setMeatAgeEnd($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setMeatDuration($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setMeatAdg($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setMeatCarcDg($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setYear($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setLocation($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setSource($worksheet->getCell($range[$c++].$row)->getValue());
					$publi->setFeedOf($worksheet->getCell($range[$c++].$row)->getValue());
					$em->persist($publi);
					$em->flush();
				}

				foreach($listChamps as $champ){
					$value = $worksheet->getCell($champ->getPosition().$row)->getValue();
					if(!empty($value)){
						$data = new Data();
						$data->setPubli($publi);
						$data->setChamp($champ);
						$data->setValue($value);

						$em->persist($data);
						$em->flush();
					}					
				}
			}
			
 			try {
 				unlink('excels/'.$fileName);
 			} catch (Exception $ex) {
 		      echo "Fail to unlink file.";
 			}
 			$upload = true;
         }

         return $this->render('upload/new.html.twig', array(
            'form' => $form->createView(),
 			'upload' => $upload
         ));
     }
	 
	private function excelColumnRange($lower, $upper) {
        ++$upper;
        for ($i = $lower; $i !== $upper; ++$i) {
            yield $i;
        }
    }

	private function getcolumnrange($min,$max){
        $output = array();
        foreach ($this->excelColumnRange($min, $max) as $value) {
            array_push($output,$value);
        }   
        return $output;
    }

    /**
    * @return string
    */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    } 
	
	private function loadPublis($repository)
	{
		$list = $repository->findAll();
		$hashTable = array();
		foreach($list as $publi)
		{
			$hashTable[$publi->getSource()][$publi->getLocation()][$publi->getYear()][$publi->getAuteur()->getId()] = $publi;
		}
		return $hashTable;
	}
	
	
/*	
	private function saveUser($name, $entityManager, $repositoryAutor, &$autorList)
    {
        //$auteur = $repositoryAutor->findOneBy(['nom' => $name]);
		if(!empty($name)){
			if(!isset($autorList[$name]) || $autorList[$name]==null){
				$auteur = new Auteur();
				$auteur->setNom($name);
								
				$entityManager->persist($auteur);
				$entityManager->flush();
				
				$autorList[$auteur->getNom()] = $auteur; 
				return $auteur;
			}else{
				return $autorList[$name];
			}
		}
		return null;		
    }
	private function savePubli($location, $year, $source, $auteur, $entityManager, $repositoryPubli, &$publiList)
    {
		if(!isset($publiList[$source][$location][$year][$auteur->getId()])){
			$publi = new Publi();
			$publi->setSource($source);
			$publi->setLocation($location);
			$publi->setYear($year);
			$publi->setAuteur($auteur);
				
			$entityManager->persist($publi);
			$entityManager->flush();
			
			$publiList[$source][$location][$year][$auteur->getId()] = $publi; 
			return $publi;
		}else{
			return $publiList[$source][$location][$year][$auteur->getId()];
		}
		
		return null;
	}
*/
}

