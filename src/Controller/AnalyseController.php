<?php

namespace App\Controller;

use App\Entity\Analyse;
use App\Form\AnalyseType;
use App\Repository\AnalyseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

/**
 * @Route("/analyse")
 */
class AnalyseController extends AbstractController
{
    /**
     * @Route("/", name="analyse_index", methods={"GET"})
     */
    public function index(AnalyseRepository $analyseRepository): Response
    {
        return $this->render('analyse/index.html.twig', [
            'analyses' => $analyseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="analyse_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $analyse = new Analyse();
        $form = $this->createForm(AnalyseType::class, $analyse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($analyse->getImage() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_directory'),$form['image']);
                $fileUploader->upload();
                $analyse->setImage($fileUploader->getFileName());
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($analyse);
            $entityManager->flush();

            return $this->redirectToRoute('analyse_index');
        }

        return $this->render('analyse/new.html.twig', [
            'analyse' => $analyse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="analyse_show", methods={"GET"})
     */
    public function show(Analyse $analyse): Response
    {
        return $this->render('analyse/show.html.twig', [
            'analyse' => $analyse,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="analyse_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Analyse $analyse): Response
    {
        $this->get('session')->set('analyse_image', ($analyse->getImage()));
        $form = $this->createForm(AnalyseType::class, $analyse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($analyse->getImage() != '')
            {
                $fileUploader= new FileUploader($this->getParameter('analyse_directory'),$form['image']);
                $fileUploader->upload();
                $analyse->setImage($fileUploader->getFileName());
                if ($this->get('session')->get('analyse_image') != '' && file_exists(__DIR__."/../../public/uploads/analyse/".$this->get('session')->get('analyse_image')))                       
                   unlink(__DIR__."/../../public/uploads/analyse/".$this->get('session')->get('analyse_image'));
            }
            // si on veut garder l'ancien fichier
            elseif (!isset($_POST["suppr"]))    $analyse->setInput($this->get('session')->get('analyse_image'));
            // si on veut supprimer l'ancien fichier
            elseif ($this->get('session')->get('analyse_image') != '' && file_exists(__DIR__."/../../public/uploads/analyse/".$this->get('session')->get('analyse_image')))                                
               unlink(__DIR__."/../../public/uploads/analyse/".$this->get('session')->get('analyse_image'));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('analyse_index');
        }

        return $this->render('analyse/edit.html.twig', [
            'analyse' => $analyse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="analyse_delete", methods={"GET"})
     */
    public function delete(Request $request, Analyse $analyse): Response
    {
        if ($analyse->getImage()!='' && file_exists(__DIR__."/../../public/uploads/analyse/".$analyse->getImage())) 
            unlink(__DIR__."/../../public/uploads/analyse/".$analyse->getImage());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($analyse);
        $entityManager->flush();

        return $this->redirectToRoute('analyse_index');
    }
}
