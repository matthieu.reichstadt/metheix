<?php

namespace App\Repository;

use App\Entity\TypeChamp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeChamp|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeChamp|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeChamp[]    findAll()
 * @method TypeChamp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeChampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeChamp::class);
    }

    // /**
    //  * @return TypeChamp[] Returns an array of TypeChamp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeChamp
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
