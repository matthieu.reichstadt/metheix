<?php

namespace App\Repository;

use App\Entity\Auteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Filter\Entity\AuteurSearch;

/**
 * @method Auteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Auteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Auteur[]    findAll()
 * @method Auteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Auteur::class);
    }

    public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(AuteurSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        if ($search->getNom())      $query = $query->andWhere('i.nom like :nom')->setParameter('nom','%'.$search->getNom().'%');

        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Auteur[] Returns an array of Auteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Auteur
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
