<?php

namespace App\Repository;

use App\Entity\AnalysePrediction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Filter\Entity\AnalysePredictionSearch;

/**
 * @method AnalysePrediction|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnalysePrediction|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnalysePrediction[]    findAll()
 * @method AnalysePrediction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnalysePredictionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnalysePrediction::class);
    }

    public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(AnalysePredictionSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }
}
