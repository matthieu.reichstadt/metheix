<?php

namespace App\Repository;

use App\Entity\AnalyseSystool;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Filter\Entity\AnalyseSystoolSearch;

/**
 * @method AnalyseSystool|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnalyseSystool|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnalyseSystool[]    findAll()
 * @method AnalyseSystool[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnalyseSystoolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnalyseSystool::class);
    }

    public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(AnalyseSystoolSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }
}
