<?php

namespace App\Repository;

use App\Entity\Equation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Equation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Equation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Equation[]    findAll()
 * @method Equation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Equation::class);
    }

    // /**
    //  * @return Equation[] Returns an array of Equation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Equation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
