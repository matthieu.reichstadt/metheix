<?php

namespace App\Repository;

use App\Entity\Champ;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Filter\Entity\ChampSearch;

/**
 * @method Champ|null find($id, $lockMode = null, $lockVersion = null)
 * @method Champ|null findOneBy(array $criteria, array $orderBy = null)
 * @method Champ[]    findAll()
 * @method Champ[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Champ::class);
    }

    public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(ChampSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        if ($search->getLibelle())  $query = $query->andWhere('i.libelle like :libelle')->setParameter('libelle',addcslashes($search->getLibelle(),'_')."%");
        if (!empty($search->getType()))   
        {
            $tabType=array();
            foreach ($search->getType() as $entityType)   array_push($tabType, $entityType->getId());
            if (!empty($tabType))                          $query = $query->andWhere('i.type in (:type)')->setParameter('type',array_values($tabType));
        }
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function findAllDisplayQuery(ChampSearch $search)
    {
        $query = $this->getAll();
        $query = $query->andWhere('i.display = :display')->setParameter('display',true);
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        if ($search->getLibelle())  $query = $query->andWhere('i.libelle like :libelle')->setParameter('libelle',addcslashes($search->getLibelle(),'_')."%");
        if (!empty($search->getType()))   
        {
            $tabType=array();
            foreach ($search->getType() as $entityType)   array_push($tabType, $entityType->getId());
            if (!empty($tabType))                          $query = $query->andWhere('i.type in (:type)')->setParameter('type',array_values($tabType));
        }
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }
    
    public function getLast5()
    {
        $query = $this->getAll();
        $query->orderBy('i.id', 'DESC')
              ->setMaxResults(5);
        return $query->getQuery()->getResult();
    }

}
