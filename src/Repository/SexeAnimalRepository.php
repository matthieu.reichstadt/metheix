<?php

namespace App\Repository;

use App\Entity\SexeAnimal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Filter\Entity\SexeAnimalSearch;

/**
 * @method SexeAnimal|null find($id, $lockMode = null, $lockVersion = null)
 * @method SexeAnimal|null findOneBy(array $criteria, array $orderBy = null)
 * @method SexeAnimal[]    findAll()
 * @method SexeAnimal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SexeAnimalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SexeAnimal::class);
    }

    public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(SexeAnimalSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())   $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        if ($search->getLibelle())   $query = $query->andWhere('i.libelle like :libelle')->setParameter('libelle','%'.$search->getLibelle().'%');
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return SexeAnimal[] Returns an array of SexeAnimal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SexeAnimal
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
