<?php

namespace App\Repository;

use App\Entity\Publi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Filter\Entity\PubliSearch;

/**
 * @method Publi|null find($id, $lockMode = null, $lockVersion = null)
 * @method Publi|null findOneBy(array $criteria, array $orderBy = null)
 * @method Publi[]    findAll()
 * @method Publi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PubliRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Publi::class);
    }

    public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(PubliSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        if ($search->getSource())   $query = $query->andWhere('i.source like :source')->setParameter('source','%'.$search->getSource().'%');
        if ($search->getYear())     $query = $query->andWhere('i.year like :year')->setParameter('year','%'.$search->getYear().'%');
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }
public function getLast5()
    {
        $query = $this->getAll();
        $query->orderBy('i.id', 'DESC')
              ->setMaxResults(5);
        return $query->getQuery()->getResult();
    }
    // /**
    //  * @return Publi[] Returns an array of Publi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Publi
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
