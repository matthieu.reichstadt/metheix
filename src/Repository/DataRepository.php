<?php

namespace App\Repository;

use App\Entity\Data;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Filter\Entity\DataSearch;
use App\Entity\Champ;

/**
 * @method Data|null find($id, $lockMode = null, $lockVersion = null)
 * @method Data|null findOneBy(array $criteria, array $orderBy = null)
 * @method Data[]    findAll()
 * @method Data[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Data::class);
    }

        public function getResults($qb)
    {
        return $qb->getQuery()->getResult();
    }   

    public function getAll()
    {
        $qb = $this->createQueryBuilder('i');      
        return $qb;
    }

    public function findAllVisibleQuery(DataSearch $search)
    {
        $query = $this->getAll();
        if ($search->getId())       $query = $query->andWhere('i.id = :id')->setParameter('id',$search->getId());
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function findVisibleQueryByPubli($type,$idPubli)
    {
        $query = $this->getAll();
        $query = $query->Join('i.champ','c');
        $query = $query->Join('c.type','t');
        $query = $query->andWhere('t.libelle = :type')->setParameter('type',$type);
        $query = $query->andWhere('i.publi = :idPubli')->setParameter('idPubli',$idPubli);
        
        $query->orderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function getLast5()
    {
        $query = $this->getAll();
        $query->orderBy('i.id', 'DESC')
              ->setMaxResults(5);
        return $query->getQuery()->getResult();
    }
}
