<?php
namespace App\Filter\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\ButtonType; 
use App\Filter\Entity\ChampGrapheSearch;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Champ;
use App\Repository\ChampRepository;

class ChampGrapheFilterType extends AbstractType
{
    /**
     * Permet de construire le formulaire
     * @param FormBuilderInterface $builder le constructeur de formulaire
     * @param array $options les options du formulaire
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('var1',EntityType::class,[
            'class' => Champ::class,
            'choice_label'=>"libelle",
            'query_builder' => function (ChampRepository $er) {return $er->createQueryBuilder('u')->orderBy('u.libelle', 'ASC');},
            'multiple' => false,
            'required'=>false,
            'attr'=> [
                'class' => "form-control select2"
            ]
        ]);
        $builder->add('var2',EntityType::class,[
            'class' => Champ::class,
            'choice_label'=>"libelle",
            'query_builder' => function (ChampRepository $er) {return $er->createQueryBuilder('u')->orderBy('u.libelle', 'ASC');},
            'multiple' => false,
            'required'=>false,
            'attr'=> [
                'class' => "form-control select2"
            ]
        ]);
        $builder->add('var3',EntityType::class,[
            'class' => Champ::class,
            'choice_label'=>"libelle",
            'query_builder' => function (ChampRepository $er) {return $er->createQueryBuilder('u')->orderBy('u.libelle', 'ASC');},
            'multiple' => false,
            'required'=>false,
            'attr'=> [
                'class' => "form-control select2"
            ]
        ]);
        
        $builder->add('filtrer', SubmitType::class, array('label' => 'Launch'));
        $builder->add('reinit', ButtonType::class, array('label' => 'Reload'));
    }
    
    /**
     * Obtenir le unique du formulaire
     * @return string le nom du formulaire
     */
    public function getBlockPrefix()
    {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ChampGrapheSearch::class,
            'method'=>'get',
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}