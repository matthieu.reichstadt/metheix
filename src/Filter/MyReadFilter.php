<?php

namespace App\Filter;

use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

/**  Define a Read Filter class implementing \PhpOffice\PhpSpreadsheet\Reader\IReadFilter  */
class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
	private $authorizeRange;
	
	function __construct()
    {
        $this->authorizeRange = $this->getcolumnrange('A','AMP');
    }
	
    public function readCell($column, $row, $worksheetName = '') {
		
        //  Read rows 1 to 7 and columns A to E only
        //if ($row >= 1 && $row <= 7) {
            if (in_array($column,$this->authorizeRange)) {
                return true;
            }
        //}
        return false;
    }
	
	function excelColumnRange($lower, $upper) {
		++$upper;
		for ($i = $lower; $i !== $upper; ++$i) {
			yield $i;
		}
	}
	
	function getcolumnrange($min,$max){
		$output = array();
		foreach ($this->excelColumnRange($min, $max) as $value) {
			array_push($output,$value);
		}	
		return $output;
	}
}