<?php

namespace App\Filter\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class ChampSearch {
	
    /**
     * @var string
     *
     */
    private $id;
    public $type;
    public $libelle;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     *
     * @return self
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param \App\Entity type
     *
     * @return self
     */
    public function addType(\App\Entity\TypeChamp $type)
    {
        $this->type[] = $type;

        return $this;
    }

}