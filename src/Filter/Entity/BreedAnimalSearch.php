<?php

namespace App\Filter\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class BreedAnimalSearch {
	
    /**
     * @var string
     *
     */
    private $id;

    /**
     * @var string
     *
     */
    private $libelle;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     *
     * @return self
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }
}