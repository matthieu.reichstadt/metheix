<?php

namespace App\Filter\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class ChampGrapheSearch {
	
    /**
     * @var string
     *
     */
    public $var1;
    public $var2;
    public $var3;

    /**
     * @return string
     */
    public function getVar1()
    {
        return $this->var1;
    }

    /**
     * @param string $var1
     *
     * @return self
     */
    public function setVar1($var1)
    {
        $this->var1 = $var1;

        return $this;
    }

    /**
     * @return string
     */
    public function getVar2()
    {
        return $this->var2;
    }

    /**
     * @param string $var2
     *
     * @return self
     */
    public function setVar2($var2)
    {
        $this->var2 = $var2;

        return $this;
    }

    /**
     * @return string
     */
    public function getVar3()
    {
        return $this->var3;
    }

    /**
     * @param string $var3
     *
     * @return self
     */
    public function setVar3($var3)
    {
        $this->var3 = $var3;

        return $this;
    }

    

}