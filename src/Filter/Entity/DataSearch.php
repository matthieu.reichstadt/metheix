<?php

namespace App\Filter\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class DataSearch {
	
    /**
     * @var string
     *
     */
    private $id;
    

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

}