<?php

namespace App\Filter\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class AuteurSearch {
	
    /**
     * @var string
     *
     */
    private $id;

    private $nom;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     *
     * @return self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

}