<?php

namespace App\Constante;

class ExcelConstante
{
   const CODE = 0;
	const AUTOR_NAME = 1;
	const PUBLI_YEAR = 18;
	const PUBLI_LOCATION = 19;
	const PUBLI_SOURCE = 20;
	const ANIMAL_BREED = 132;
	const ANIMAL_SEXE = 133;
	
	/******************CONST ANIMAL************************/
	const ANIMAL_OMD = 90;
	const ANIMAL_ND = 91;
	const ANIMAL_ND_METHOD = 92;
	const ANIMAL_NDFD = 93;
	const ANIMAL_CFD = 95;
	const ANIMAL_DIGESTIBILITY_METHOD = 97;
	const ANIMAL_MILKF = 112;
	const ANIMAL_MILKCP = 113;
	const ANIMAL_MILK_D = 115;
	const ANIMAL_CH4 = 130;
	const ANIMAL_CH4M = 131;
	const ANIMAL_DMI_DIGETIBIILITY = 156;
	const ANIMAL_Y_OMD = 295;
	const ANIMAL_Y_ND = 242;
	const ANIMAL_Y_ND_METH = 244;
	const ANIMAL_Y_NDFD = 245;
	const ANIMAL_Y_DIG_METH = 247;
	const ANIMAL_Y_MILKF = 248;
	const ANIMAL_Y_MILKCP = 249;
	const ANIMAL_Y_MILK_D = 250;
	const ANIMAL_Y_CH4 = 251;
	const ANIMAL_Y_CH4_METH = 253;
	const ANIMAL_Y_STAD = 255;
	const ANIMAL_Y_NI_GD = 257;
	const ANIMAL_Y_NDI_GD = 258;
	const ANIMAL_Y_FECAL_N_GD = 259;
	const ANIMAL_Y_URINARY_N_GD = 260;
	const ANIMAL_Y_DOMI_GD = 269;
	const ANIMAL_Y_FECAL_OM_GD = 270;

	//array(90,91,92,93,95);
	const ANIMAL_FIELDS = array(
		self::ANIMAL_OMD,
		self::ANIMAL_ND,
		self::ANIMAL_ND_METHOD,
		self::ANIMAL_NDFD,
		self::ANIMAL_CFD,
		self::ANIMAL_DIGESTIBILITY_METHOD,
		self::ANIMAL_MILKF,
		self::ANIMAL_MILKCP,
		self::ANIMAL_MILK_D,
		self::ANIMAL_CH4,
		self::ANIMAL_CH4M,
		self::ANIMAL_DMI_DIGETIBIILITY,
		self::ANIMAL_Y_OMD,
		self::ANIMAL_Y_ND,
		self::ANIMAL_Y_ND_METH,
		self::ANIMAL_Y_NDFD,
		self::ANIMAL_Y_DIG_METH,
		self::ANIMAL_Y_MILKF,
		self::ANIMAL_Y_MILKCP,
		self::ANIMAL_Y_MILK_D,
		self::ANIMAL_Y_CH4,
		self::ANIMAL_Y_CH4_METH,
		self::ANIMAL_Y_STAD,
		self::ANIMAL_Y_NI_GD,
		self::ANIMAL_Y_NDI_GD,
		self::ANIMAL_Y_FECAL_N_GD,
		self::ANIMAL_Y_URINARY_N_GD,
		self::ANIMAL_Y_DOMI_GD,
		self::ANIMAL_Y_FECAL_OM_GD
	);
	
	const DIET_DIET = 22;
	const DIET_FORDM = 23;
	const DIET_FORAGE_SPECIES = 24;
	const DIET_FORAGE_PRESERVATION_METHOD = 26;
	const DIET_CONDM = 47;
	const DIET_CONTYPE = 48;
	const DIET_PERC_CONCENT = 49;
	const DIET_DMI = 51;
	const DIET_CP = 53;
	const DIET_EE = 54;
	const DIET_EE_METHOD = 55;
	const DIET_OM = 57;
	const DIET_NDF = 58;
	const DIET_Y_DIET = 182;
	const DIET_Y_FORDM = 183;
	const DIET_Y_FORAGE_SPECIE = 198;
	const DIET_Y_FORAGE_PRES_METH = 188;
	const DIET_Y_CONDM = 219;
	const DIET_Y_CONTYPE = 220;
	const DIET_Y_PCO = 222;
	const DIET_Y_DMI = 224;
	const DIET_Y_CP = 226;
	const DIET_Y_EE = 228;
	const DIET_Y_EE_METH = 230;
	const DIET_Y_OM = 231;
	const DIET_Y_NDF = 233;
	const DIET_Y_STA = 237;
	const DIET_Y_OMI_GD = 268;
	const DIET_INGREDIENTS = 441;
	const DIET_FOUR = 442;
	const DIET_CON_COP =588;
	
	//array(25,26,27,29);
	const DIET_FIELDS = array(
		self::DIET_DIET,
		self::DIET_FORDM,
		self::DIET_FORAGE_SPECIES,
		self::DIET_FORAGE_PRESERVATION_METHOD,
		self::DIET_CONDM,
		self::DIET_CONTYPE,
		self::DIET_PERC_CONCENT,
		self::DIET_DMI,
		self::DIET_CP,
		self::DIET_EE,
		self::DIET_EE_METHOD,
		self::DIET_OM,
		self::DIET_NDF,
		self::DIET_Y_DIET,
		self::DIET_Y_FORDM,
		self::DIET_Y_FORAGE_SPECIE,
		self::DIET_Y_FORAGE_PRES_METH,
		self::DIET_Y_CONDM,
		self::DIET_Y_CONTYPE,
		self::DIET_Y_PCO,
		self::DIET_Y_DMI,
		self::DIET_Y_CP,
		self::DIET_Y_EE,
		self::DIET_Y_EE_METH,
		self::DIET_Y_OM,
		self::DIET_Y_NDF,
		self::DIET_Y_STA,
		self::DIET_Y_OMI_GD,
		self::DIET_INGREDIENTS,
		self::DIET_FOUR,
		self::DIET_CON_COP
	);
}
