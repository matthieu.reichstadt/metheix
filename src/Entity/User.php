<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use FR3D\LdapBundle\Model\LdapUserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser implements LdapUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Ldap Object Distinguished Name
     * @ORM\Column(type="string", length=128, nullable=true)
     * @var string $dn
     */
    private $dn;

    /**
     * @ORM\OneToMany(targetEntity=AnalyseSystool::class, mappedBy="user")
     */
    private $analyseSystools;

    /**
     * @ORM\OneToMany(targetEntity=AnalysePrediction::class, mappedBy="user")
     */
    private $analysePredictions;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->analyseSystools = new ArrayCollection();
        $this->analysePredictions = new ArrayCollection();
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string $dn
     */
    public function getDn(): ?string
    {
        return $this->dn;
    }

    /**
     * @param string $dn $dn
     *
     * @return self
     */
    public function setDn($dn)
    {
        $this->dn = $dn;

        return $this;
    }

    /**
     * @return Collection|AnalyseSystool[]
     */
    public function getAnalyseSystools(): Collection
    {
        return $this->analyseSystools;
    }

    public function addAnalyseSystool(AnalyseSystool $analyseSystool): self
    {
        if (!$this->analyseSystools->contains($analyseSystool)) {
            $this->analyseSystools[] = $analyseSystool;
            $analyseSystool->setUser($this);
        }

        return $this;
    }

    public function removeAnalyseSystool(AnalyseSystool $analyseSystool): self
    {
        if ($this->analyseSystools->removeElement($analyseSystool)) {
            // set the owning side to null (unless already changed)
            if ($analyseSystool->getUser() === $this) {
                $analyseSystool->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AnalysePrediction[]
     */
    public function getAnalysePredictions(): Collection
    {
        return $this->analysePredictions;
    }

    public function addAnalysePredictions(AnalysePrediction $analysePredictions): self
    {
        if (!$this->analysePredictions->contains($analysePredictions)) {
            $this->analysePredictions[] = $analysePredictions;
            $analysePredictions->setUser($this);
        }

        return $this;
    }

    public function removeAnalysePredictions(AnalysePrediction $analysePredictions): self
    {
        if ($this->analysePredictions->removeElement($analysePredictions)) {
            // set the owning side to null (unless already changed)
            if ($analysePredictions->getUser() === $this) {
                $analysePredictions->setUser(null);
            }
        }

        return $this;
    }
}