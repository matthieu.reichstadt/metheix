<?php

namespace App\Entity;

use App\Repository\EquationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EquationRepository::class)
 */
class Equation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $equation;


    public function __toString()
    {
        return $this->label;
    }
    
    public function __construct()
    {
        $this->date = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getEquation(): ?string
    {
        return $this->equation;
    }

    public function setEquation(string $equation): self
    {
        $this->equation = $equation;

        return $this;
    }

}
