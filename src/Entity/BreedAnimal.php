<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BreedAnimal
 *
 * @ORM\Table(name="breed_animal")
 * @ORM\Entity
 */
class BreedAnimal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=100, nullable=false)
     */
    private $libelle;

    public function __toString() { return strval($this->libelle);}
	public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }


}
