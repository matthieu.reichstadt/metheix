<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Publi
 *
 * @ORM\Table(name="publi", indexes={@ORM\Index(name="auteur", columns={"auteur"})})
 * @ORM\Entity
 */
class Publi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=1000, nullable=true)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=500, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=10, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $auteur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $groupe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $datatype;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pedigree;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $exp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $n;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $species;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dietcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dim;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meatAgeStart;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meatAgeEnd;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meatDuration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meatADG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meatCarcDG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $feedof;

    /**
     * @ORM\OneToMany(targetEntity=Data::class, mappedBy="publi")
     */
    private $data;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    public function __toString() { return strval($this->id);}
	public function getId(): ?int
     {
         return $this->id;
     }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;
        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getGroupe(): ?string
    {
        return $this->groupe;
    }

    public function setGroupe(?string $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(?string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDatatype(): ?string
    {
        return $this->datatype;
    }

    public function setDatatype(?string $datatype): self
    {
        $this->datatype = $datatype;

        return $this;
    }

    public function getPedigree(): ?string
    {
        return $this->pedigree;
    }

    public function setPedigree(?string $pedigree): self
    {
        $this->pedigree = $pedigree;

        return $this;
    }

    public function getTrial(): ?string
    {
        return $this->trial;
    }

    public function setTrial(?string $trial): self
    {
        $this->trial = $trial;

        return $this;
    }

    public function getExp(): ?string
    {
        return $this->exp;
    }

    public function setExp(?string $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getTrt(): ?string
    {
        return $this->trt;
    }

    public function setTrt(?string $trt): self
    {
        $this->trt = $trt;

        return $this;
    }

    public function getN(): ?string
    {
        return $this->n;
    }

    public function setN(?string $n): self
    {
        $this->n = $n;

        return $this;
    }

    public function getSpecies(): ?string
    {
        return $this->species;
    }

    public function setSpecies(?string $species): self
    {
        $this->species = $species;

        return $this;
    }

    public function getDietcode(): ?string
    {
        return $this->dietcode;
    }

    public function setDietcode(?string $dietcode): self
    {
        $this->dietcode = $dietcode;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDim(): ?string
    {
        return $this->dim;
    }

    public function setDim(?string $dim): self
    {
        $this->dim = $dim;

        return $this;
    }

    public function getMeatAgeStart(): ?string
    {
        return $this->meatAgeStart;
    }

    public function setMeatAgeStart(?string $meatAgeStart): self
    {
        $this->meatAgeStart = $meatAgeStart;

        return $this;
    }

    public function getMeatAgeEnd(): ?string
    {
        return $this->meatAgeEnd;
    }

    public function setMeatAgeEnd(?string $meatAgeEnd): self
    {
        $this->meatAgeEnd = $meatAgeEnd;

        return $this;
    }

    public function getMeatDuration(): ?string
    {
        return $this->meatDuration;
    }

    public function setMeatDuration(?string $meatDuration): self
    {
        $this->meatDuration = $meatDuration;

        return $this;
    }

    public function getMeatADG(): ?string
    {
        return $this->meatADG;
    }

    public function setMeatADG(?string $meatADG): self
    {
        $this->meatADG = $meatADG;

        return $this;
    }

    public function getMeatCarcDG(): ?string
    {
        return $this->meatCarcDG;
    }

    public function setMeatCarcDG(?string $meatCarcDG): self
    {
        $this->meatCarcDG = $meatCarcDG;

        return $this;
    }

    public function getFeedof(): ?string
    {
        return $this->feedof;
    }

    public function setFeedof(?string $feedof): self
    {
        $this->feedof = $feedof;

        return $this;
    }

    /**
     * @return Collection|Data[]
     */
    public function getData(): Collection
    {
        return $this->data;
    }

    public function addData(Data $data): self
    {
        if (!$this->data->contains($data)) {
            $this->data[] = $data;
            $data->setPubli($this);
        }

        return $this;
    }

    public function removeData(Data $data): self
    {
        if ($this->data->removeElement($data)) {
            // set the owning side to null (unless already changed)
            if ($data->getPubli() === $this) {
                $data->setPubli(null);
            }
        }

        return $this;
    }

}
