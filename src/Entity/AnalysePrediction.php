<?php

namespace App\Entity;

use App\Repository\AnalysePredictionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnalysePredictionRepository::class)
 */
class AnalysePrediction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=AnalyseSystool::class, inversedBy="analysePredictions")
     */
    private $analyseSystool;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $input;

    /**
     * @ORM\ManyToOne(targetEntity=Equation::class, inversedBy="date")
     */
    private $equation;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="analysePredictions")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sortie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    public function __toString()
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnalyseSystool(): ?AnalyseSystool
    {
        return $this->analyseSystool;
    }

    public function setAnalyseSystool(?AnalyseSystool $analyseSystool): self
    {
        $this->analyseSystool = $analyseSystool;

        return $this;
    }

    public function getInput(): ?string
    {
        return $this->input;
    }

    public function setInput(string $input=null): self
    {
        $this->input = $input;

        return $this;
    }

    public function getEquation(): ?Equation
    {
        return $this->equation;
    }

    public function setEquation(?Equation $equation): self
    {
        $this->equation = $equation;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSortie(): ?string
    {
        return $this->sortie;
    }

    public function setSortie(string $sortie): self
    {
        $this->sortie = $sortie;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

}
