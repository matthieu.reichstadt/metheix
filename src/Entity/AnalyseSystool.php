<?php

namespace App\Entity;

use App\Repository\AnalyseSystoolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnalyseSystoolRepository::class)
 */
class AnalyseSystool
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aliment;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="analyseSystools")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sortie;

    /**
     * @ORM\OneToMany(targetEntity=AnalysePrediction::class, mappedBy="analyseSystool")
     */
    private $analysePredictions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    public function __toString()
    {
        return $this->nom;
    }

    public function __construct()
    {
        $this->analysePredictions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRation(): ?string
    {
        return $this->ration;
    }

    public function setRation(string $ration=null): self
    {
        $this->ration = $ration;

        return $this;
    }

    public function getAliment(): ?string
    {
        return $this->aliment;
    }

    public function setAliment(string $aliment=null): self
    {
        $this->aliment = $aliment;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSortie(): ?string
    {
        return $this->sortie;
    }

    public function setSortie(string $sortie): self
    {
        $this->sortie = $sortie;

        return $this;
    }

    /**
     * @return Collection|AnalysePrediction[]
     */
    public function getAnalysePredictions(): Collection
    {
        return $this->analysePredictions;
    }

    public function addAnalysePrediction(AnalysePrediction $analysePrediction): self
    {
        if (!$this->analysePredictions->contains($analysePrediction)) {
            $this->analysePredictions[] = $analysePrediction;
            $analysePrediction->setAnalyseSystool($this);
        }

        return $this;
    }

    public function removeAnalysePrediction(AnalysePrediction $analysePrediction): self
    {
        if ($this->analysePredictions->removeElement($analysePrediction)) {
            // set the owning side to null (unless already changed)
            if ($analysePrediction->getAnalyseSystool() === $this) {
                $analysePrediction->setAnalyseSystool(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
