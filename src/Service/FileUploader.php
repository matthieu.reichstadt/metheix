<?php
// src/Service/FileUploader.php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class FileUploader
{
    private $targetDirectory;
    private $uploadedFile;
    private $fileName;

    public function __construct($targetDirectory,$obj)
    {
        $this->targetDirectory = $targetDirectory;
        $this->uploadedFile=$obj->getData();
        $this->fileName=$this->uploadedFile->getClientOriginalName();
    }

    public function upload()
    {
        try {
            $this->uploadedFile->move($this->getTargetDirectory(), $this->fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }
    }

    /**
     * @return mixed
     */
    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    /**
     * @param mixed $targetDirectory
     *
     * @return self
     */
    public function setTargetDirectory($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUploadedFile()
    {
        return $this->uploadedFile;
    }

    /**
     * @param mixed $uploadedFile
     *
     * @return self
     */
    public function setUploadedFile($uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     *
     * @return self
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }
}