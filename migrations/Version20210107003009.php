<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210107003009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE animal (id INT AUTO_INCREMENT NOT NULL, breed_animal INT DEFAULT NULL, sexe_animal INT DEFAULT NULL, champs_animal INT DEFAULT NULL, publi INT DEFAULT NULL, code VARCHAR(100) NOT NULL, valeur VARCHAR(100) NOT NULL, INDEX IDX_6AAB231FD564A565 (publi), INDEX champs_animal (champs_animal), INDEX breed_animal (breed_animal), INDEX sexe_animal (sexe_animal), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auteur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(200) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE breed_animal (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE champs_animal (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE champs_diet (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diet (id INT AUTO_INCREMENT NOT NULL, publi INT DEFAULT NULL, champs_diet INT DEFAULT NULL, code VARCHAR(100) NOT NULL, valeur VARCHAR(100) NOT NULL, INDEX IDX_9DE46520D564A565 (publi), INDEX champs_diet (champs_diet), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', dn VARCHAR(128) DEFAULT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE publi (id INT AUTO_INCREMENT NOT NULL, auteur INT DEFAULT NULL, source VARCHAR(1000) DEFAULT NULL, location VARCHAR(500) DEFAULT NULL, year VARCHAR(10) DEFAULT NULL, INDEX auteur (auteur), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sexe_animal (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231F629D61B6 FOREIGN KEY (breed_animal) REFERENCES breed_animal (id)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231FE60D9EDA FOREIGN KEY (sexe_animal) REFERENCES sexe_animal (id)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231FFBD86298 FOREIGN KEY (champs_animal) REFERENCES champs_animal (id)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231FD564A565 FOREIGN KEY (publi) REFERENCES publi (id)');
        $this->addSql('ALTER TABLE diet ADD CONSTRAINT FK_9DE46520D564A565 FOREIGN KEY (publi) REFERENCES publi (id)');
        $this->addSql('ALTER TABLE diet ADD CONSTRAINT FK_9DE4652074A68BE FOREIGN KEY (champs_diet) REFERENCES champs_diet (id)');
        $this->addSql('ALTER TABLE publi ADD CONSTRAINT FK_D564A56555AB140 FOREIGN KEY (auteur) REFERENCES auteur (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE publi DROP FOREIGN KEY FK_D564A56555AB140');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231F629D61B6');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231FFBD86298');
        $this->addSql('ALTER TABLE diet DROP FOREIGN KEY FK_9DE4652074A68BE');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231FD564A565');
        $this->addSql('ALTER TABLE diet DROP FOREIGN KEY FK_9DE46520D564A565');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231FE60D9EDA');
        $this->addSql('DROP TABLE animal');
        $this->addSql('DROP TABLE auteur');
        $this->addSql('DROP TABLE breed_animal');
        $this->addSql('DROP TABLE champs_animal');
        $this->addSql('DROP TABLE champs_diet');
        $this->addSql('DROP TABLE diet');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE publi');
        $this->addSql('DROP TABLE sexe_animal');
    }
}
